"""
In the 20x20 grid below, four numberes along a diagonal line have
been marked in red

The product of these numbers is 26x63x78x14 = 1788696

What is the greatest product of four adjacent numbers in the same
direction (up, down, left, right and diagonally) in the 20x20 grid?

"""

from random import randint
from pprint import pprint

x = 5
y = 5
grid = []


d = "08 02 22 97 38 15 00 40 00 75 04 05 07 78 52 12 50 77 91 08 49 49 99 40 17 81 18 57 60 87 17 40 98 43 69 48 04 56 62 00 81 49 31 73 55 79 14 29 93 71 40 67 53 88 30 03 49 13 36 65 52 70 95 23 04 60 11 42 69 24 68 56 01 32 56 71 37 02 36 91 22 31 16 71 51 67 63 89 41 92 36 54 22 40 40 28 66 33 13 80 24 47 32 60 99 03 45 02 44 75 33 53 78 36 84 20 35 17 12 50 32 98 81 28 64 23 67 10 26 38 40 67 59 54 70 66 18 38 64 70 67 26 20 68 02 62 12 20 95 63 94 39 63 08 40 91 66 49 94 21 24 55 58 05 66 73 99 26 97 17 78 78 96 83 14 88 34 89 63 72 21 36 23 09 75 00 76 44 20 45 35 14 00 61 33 97 34 31 33 95 78 17 53 28 22 75 31 67 15 94 03 80 04 62 16 14 09 53 56 92 16 39 05 42 96 35 31 47 55 58 88 24 00 17 54 24 36 29 85 57 86 56 00 48 35 71 89 07 05 44 44 37 44 60 21 58 51 54 17 58 19 80 81 68 05 94 47 69 28 73 92 13 86 52 17 77 04 89 55 40 04 52 08 83 97 35 99 16 07 97 57 32 16 26 26 79 33 27 98 66 88 36 68 87 57 62 20 72 03 46 33 67 46 55 12 32 63 93 53 69 04 42 16 73 38 25 39 11 24 94 72 18 08 46 29 32 40 62 76 36 20 69 36 41 72 30 23 88 34 62 99 69 82 67 59 85 74 04 36 16 20 73 35 29 78 31 90 01 74 31 49 71 48 86 81 16 23 57 05 54 01 70 54 71 83 51 54 69 16 92 33 48 61 43 52 01 89 19 67 48"

d = d.split(" ")
l=[]
for i in d:
    l.append(int(i))
d=l
k=[]
for i in range(20):
    k.append(d[i*20:(i+1)*20])

print(k)
print(len(k))
print(len(k[0]))
print(k[0][2])


def drawGrid(x,y):
    g = []
    c = []

    for j in range(x):
        c=[]
        for k in range(y):
            c.append(randint(0,99))
        g.append(c)
    return g

def product(l):
    i=1
    for n in l:
        i*=n
    return i

def clean(l):
    ## cleans a list of zeros and 1's
    if len(set(l).intersection({0})) > 0: return [0]
    l = [i for i in l if l !=1]
    return l

def inBounds(g,y,x,m,s):
    g_y_len = len(g)
    g_x_len = len(g[0])

    h = ((y+s)*m[0] <=g_y_len*m[0])
    v = ((x+s)*abs(m[1])<=g_x_len*abs(m[1]))
    d = (x+1+(s*m[1])*(m[0]*abs(m[1]))>=0)
    ##print('h: ' +str(h)+'v: ' +str(v)+'d: ' +str(d))
    return h & v & d

def getDigits(g,y,x,m,s):
    l = []
    for i in range(s):
        l.append(g[y+m[0]*i][x+m[1]*i])
    #print(m + l)
    return(l)

def crawlGrid(grid, s):
    horiz = [0,1]
    fdown = [1,1]
    verts = [1,0]
    bdown = [1,-1]
    directions = [horiz,fdown,verts,bdown]

    g_y_len = len(grid)
    g_x_len = len(grid[0])

    lc = 0
    lxy= [0]
    ld = [0]
    ll = [0]

    for yi in range(g_y_len):
        for xi in range(g_x_len):
            for d in directions:
                if inBounds(grid,yi,xi,d,s):
##                    print('direction: '+str(d)+' is in bounds')
                    digits = clean(getDigits(grid,yi,xi,d,s))
                    ##digits = getDigits(grid,yi,xi,d,s)
##                    print(digits)
                    d_prod = product(digits)
                    if d_prod >lc:
                        lc=d_prod
                        lxy= [yi,xi]
                        ld = d
                        ll = digits
                #else:print('y:' + str(yi) + ' x:' + str(xi) + 'direction: '+str(d)+' is out of bounds')
    return([lc,lxy,ld,ll])

##grid = drawGrid(x,y)
##pprint(grid, width=25)
##print('-----')
solution=crawlGrid(k, 4)
print('-----')
print(solution)
