"""
You are given the following information, but you may prefer to do
some research for yourself.

1 Jan 1900 was a Monday.
Thirty days has September,
April, June and November.
All the rest have thirty-one,
Saving February alone,
Which has twenty-eight, rain or shine.
And on leap years, twenty-nine.
A leap year occurs on any year evenly divisible by 4, but not on a
century unless it is divisible by 400.
How many Sundays fell on the first of the month during the
twentieth century (1 Jan 1901 to 31 Dec 2000)?

"""


dofw=0
totalDays=365
totalSundays=0
m = {1:31, 2:28, 3:31, 4:30, 5:31,
     6:30, 7:31, 8:31, 9:30, 10:31,
     11:30, 12:31,
     }

def leap(yn,yp):
    l,i=0,(yn-yp)%4
    c=0
    while ((l<1) & (i>=0)):
        l= (1-bool((yn-i)%4)) | (1-bool((yp+i)%4))
        i-=1
    for y in range((yp//100),(yn//100)+1):
        if (yp<=(y*100)<=yn) & (y%4>0):
            c+=1
    return (((yn-yp)//4)+l-c)

def leapCheck(y):
    if y%100<1:
        if (y//100)%4<1:return 1
    elif y%4<1:
        return 1
    return 0

def dayOfWeek(y,mn,d):
    st=1900
    stm=12
    # how many months since start
    _m = (mn-1)+(12*(y-st))

    ## go through each month from the start
    ## start on a sunday, and if it's divisible by sunday, add it
    c=0
    td=0
    _y=0
    
    first=True
    for i in range(_m+1):
        _y=i//12
        i=i%12
        if i==2: l=leapCheck(_y+y)
        else: l=0
        if first:
            c+=m.get(i+1)-6+l
            first=False
        elif stm>0:
            print('month')
            stm-=1
            c+=m.get(i+1)+l
        else:
            c+=m.get(i+1)+l
        if (c%7<1) & (stm<1):
            td+=1
        #print('total sundays: %i' %(td))
    ##last check, days left over
    c+= d-1
    if c%7<1:
            td+=1
            
    print(td)
    return td


td= dayOfWeek(2000,12,31)
print(td)
