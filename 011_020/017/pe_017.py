"""
If the numbers 1 to 5 are written out in words: one, two, three, four, five,
then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

If all the numbers from 1 to 1000 (one thousand) inclusive were written out
in words, how many letters would be used?


NOTE: Do not count sp aces or hyphens. For example, 342 (three hundred and
forty-two) contains 23 letters and 115 (one hundred and fifteen) contains
20 letters. The use of "and" when writing out numbers is in compliance with
British usage.

"""
s=1
e=1000

singles={0:'',1:'one',2:'two',3:'three',4:'four',5:'five',6:'six',7:'seven',8:'eight',9:'nine',10:'ten',11:'eleven',12:'twelve', 13:'thirteen', 14:'fourteen',15:'fifteen',16:'sixteen',17:'seventeen', 18:'eighteen',19:'nineteen'}
tens={1:'teen',2:'twenty',3:'thirty',4:'forty',5:'fifty',6:'sixty',7:'seventy',8:'eighty', 9:'ninety', 0:'and'}


def psingles(n,b=0):
    if len(n)<1: return('')
    n=int(n[len(n)-(1+b):])
    return singles.get(int(n))

def ptens(n):
    if len(n)<2:return('')
    n=int(n[len(n)-2])
    return tens.get(int(n))

def phundreds(n):
    a=''
    if len(n)<3:return('')
    if int(n[len(n)-2:])>0: a='and'
    n=int(n[len(n)-3])
    if int(n)<1:return('')
    return (singles.get(int(n))+'hundred'+a)

def pthousands(n):
    if len(n)<4:return('')
    n=int(n[len(n)-4])
    return(singles.get(int(n))+'thousand')

def printNumber(i):
    th,hu,te,si='','','',''
    n = str(i)
    if int(n[len(n)-2:])<20:
        si=psingles(n,1)
    else:
        si=psingles(n,0)
        te=ptens(n)
    hu=phundreds(n)
    th=pthousands(n)
    return(th+hu+te+si)
n=0
for i in range(s,e+1):
    j=printNumber(i)
    print(j)
    n+=len(j)
    if i == 1000:
        print('%i: %s' %(len(j),j))
print(n)
