"""
By starting at the top of the triangle below and moving to adjacent numbers
on the row below, the maximum total from top to bottom is 23.

   3
  7 4
 2 4 6
8 5 9 3

That is, 3 + 7 + 4 + 9 = 23.

Find the maximum total from top to bottom of the triangle below:

              75
             95 64
            17 47 82
           18 35 87 10
          20 04 82 47 65
         19 01 23 75 03 34
        88 02 77 73 07 63 67
       99 65 04 28 06 16 70 92
      41 41 26 56 83 40 80 70 33
     41 48 72 33 47 32 37 16 94 29
    53 71 44 65 25 43 91 52 97 51 14
   70 11 33 28 77 73 17 78 39 68 17 57
  91 71 52 38 17 14 91 43 58 50 27 29 48
 63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23

NOTE: As there are only 16384 routes, it is possible to solve this problem
by trying every route. However, Problem 67, is the same challenge with a
triangle containing one-hundred rows; it cannot be solved by brute force,
and requires a clever method! ;o)

"""
from random import randint
## solutions:
## find biggest number?
## flatten?
## work backkwards?
## work backwards, take the biggest option

#tri=[[3],[7,4],[2,4,6],[8,5,9,3]]
tri="""              75
             95 64
            17 47 82
           18 35 87 10
          20 04 82 47 65
         19 01 23 75 03 34
        88 02 77 73 07 63 67
       99 65 04 28 06 16 70 92
      41 41 26 56 83 40 80 70 33
     41 48 72 33 47 32 37 16 94 29
    53 71 44 65 25 43 91 52 97 51 14
   70 11 33 28 77 73 17 78 39 68 17 57
  91 71 52 38 17 14 91 43 58 50 27 29 48
 63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23"""

def parseTriangle(s):
    new=[]
    s=s.split("\n")
#    s=s.replace(" ",".")
    for v in s:
        v=v.split(" ")
        v=[int(i) for i in v if i != ""]
        print(v)
        new.append(v)
    return(new)

def buildTriangle(n=3):
    ## n=depth
    t=[]
    for i in range(1,n+1):
        r=[]
        for j in range(i):
            a=str(randint(0,9))
            b=str(randint(0,9))
            r.append(int(a+b))
##        print(r)
        t.append(r)
    print(t)
    return(t)

def reverseFlattenSearch(tri):
    flat=[]
    rev= tri[::-1]
    ## this line should be sometihng different...
    r=rev[0]
    j=0
    while len(r)>1:
        _r=[]
        print('r:')
        print(r)
        for i,v in enumerate(rev[j+1]):
            print('adding %i with %i and %i' %(v, r[i], r[i+1]))
            a= v+r[i]
            b= v+r[i+1]
            if a>b:_r.append(a)
            else:_r.append(b)
        j+=1
        print('-----')
        print('updating r')
        r=_r
    return(r)


#tri=buildTriangle(4)
tri=parseTriangle(tri)
s=reverseFlattenSearch(tri)
print(s)
#1074
