"""
The following iterative sequence is defined for the set of positive integers:
n-> n/2 (n is even)
n-> 3n+1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:

13->40->20->10->5->16->8->4->2->1

It can be seen that this sequence (starting at 13 and finishing at 1) contains
10 terms.
Although it has not been proved yet (Collatz Problem), it is thought that all
starting numbeers finish at 1.

Which starting number, under one million, produces the longest chain?

Note: once the chain starts the terms are allowed to go above one million

"""
#-----
#the integer below 1000000 which produces the longest chain is 837799, which contains 525 terms
#[Finished in 127.093s]

#the integer below 1000000 which produces the longest chain is 837799, which contains 525 terms
#[Finished in 29.982s]

d = 0
limit = 1000000

##longest_chain
ld = 0
lc = 0
def fullChainLength(n):
    c=0
    while n>1:
        if n%2>0:
            n=int((n*3+1)/2)
            c+=2
        else:
            n=int(n/2)
            c+=1
    return c+1

for i in range(limit):
    c = fullChainLength(i)
    if c>lc:
        lc=c
        ld=i
    #print('%i: %i' %(i,fullChainLength(i)))

print('-----')
print('the integer below %i which produces the longest chain is %i, which contains %i terms' %(limit,ld,lc))
