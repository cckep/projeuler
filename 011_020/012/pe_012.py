"""
The sequence of triangle numbes is generating by adding the natural numbers.
So the 7th triangle number would be 1+2+3+4+5+6+7 = 28. The first ten terms
would be:

1,3,6,10,15,21,28,36,45,55

Let us list the factors of the first seven triangle numbers:

 1: 1
 2: 1,3
 6: 1,2,3,6
10: 1,2,5,10
15: 1,3,5,15
21: 1,3,7,21
28: 1,2,4,7,14,28

We can see 28 is the first friangle number to have over five divisors.

What is the value of the first triangle number to have over 500 divisors.
"""


n=0
t=0
unsolved = True
limit = 500

def factor(n):
    i=0
    f=[]
    t=n
    while t>i:
        i+=1
        if n%i<1:
            f.append(i)
            f.append(int(n/i))
            t=int(n/i)
    f = list(set(f))
    return(f)

while unsolved:
    n+=1
    t+=n
    f = factor(t)
    unsolved = (len(f) < limit)
print('triangle %i: is %i with %i factors' %(n,t,len(f)))
print(f)

## triangle 12375: is 76576500 with 576 factors
