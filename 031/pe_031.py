"""
In England the currency is made up of pound, L, and pence, p, and there are
eight coins in general circulation:

1p,2p,5p,10p,20p, 50p,1L(100p), and 2L(200p)

It is possible to make 2L in the following way:

1x1L + 1x50p + 2x20p + 1x5p + 1x2p + 3x1p

How many different ways can 2L be made using any combination of coins?

"""
c = [200, 100, 50, 10, 5, 2, 1]

check = 200
total = 1 #200 coin by itself

for f in range(200, -1, -100):
    p = 200-(f)
    for g in range(200,-1,-50):
        l = 200-(f+g)
        for x in range(l-l%20, -1, -20):
            z = 200-(f+g+x)
            for h in range(z-z%10,-1,-10):
                m = 200-(f+g+h+x)
                for i in range(m-m%5, -1, -5):
                    n = 200-(f+g+h+i+x)
                    for j in range(n-n%2, -1, -2):
                        k = 200-(f+g+h+i+j+x)
                        if (k >= 0):
                            print(f,g,x,h,i,j,k)
                            total+=1

print(total)
