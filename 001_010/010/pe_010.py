"""
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.

"""

## this still takes forever

upperLimit = 2000000

def primeCheck(n):
    ## even check
    if n%2<1: return False
    ## only check below square root!
    l = int(n**0.5)+1
    for i in range(3,l,2):
        if n%i <1: return False
    return True


def searchDigits(n):
    s = 3
    primes = [2,]
    
    while s < n:
        if primeCheck(s):
            #print('prime!')
            primes.append(s)
        s+=2
    return primes


solution = searchDigits(upperLimit)

#print(solution)
print(sum(solution))
