"""
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13,
we can see that the 6th prime is 13.

What is the 10 001st prime number?
"""

## i think theres a faster way to do this...

c = 0
n = -1
stopPoint = 10001
solution = 0

def checkPrime(n):
    ## if n is even return false
    if n%2<1: return False
    ## 3 is the first valid number, n-2 is the last search term
    for i in range(3,n-2,2):
        ## return false if i is a multiple of n
        if n%i < 1: return False
    ## it is prime
    return True

while solution < 1:
    n+=2
    if checkPrime(n): c+=1
    if c >= stopPoint: solution = n
    

print (c)
print('solution: %i' %(n))

