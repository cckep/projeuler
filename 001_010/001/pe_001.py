"""
If we list all the natural numbers below 10 that are multiples of 3 or 5,
we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
"""
#233168

## get all multiples of 3 in set
## get all multiples of 5 in set

## sum merged set


limit = 1_000
mult = {3,5}

def iterate(m, limit):
    num_set=set()
    for n in range(limit):
        for m in mult:
            if n%m < 1:
                num_set.add(n)
                break
    return num_set


def multiples(m, limit):
    s=set()
    for i in m:
        s=s.union(set([n for n in range(0,limit,i)]))
    return s

solution=multiples(mult,limit)
print(sum(solution))
solution=iterate(mult,limit)
print(sum(solution))
