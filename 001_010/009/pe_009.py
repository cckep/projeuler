"""
A Pythagorean triplet is a set if three natural numbers, a<b<c, for which
a^2+b^2 = c^2

For example 3^2 + 4^2 = 9+16 = 25 = 5^2

There exists exactly on Pythagorean triplet for which a+b+c = 1000
Find the product of abc

"""

## a + b + c = 3+4+5 = 12
## a + b + c = 1000

s = 1000

def checkPythagorean(a,b,c,s):
    if a+b+c == s:
        if a*a + b*b == c*c:
            return True
    return False

def pythagoreanTriplet(s):
    for a in range(1,s+1):
        for b in range(a+1,s+1):
            c = s-(a+b)
            if c > b: ## move a + b +c = s into here...
                if checkPythagorean(a,b,c,s):
                    return [a,b,c]
    return[0,0,0]
        

p = pythagoreanTriplet(s)
## validate against zeros
print(p)

v=1
for i in p:
    v*=i
print(v)
