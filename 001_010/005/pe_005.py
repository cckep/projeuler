"""
2520 is the smallest number that can be divided by each of the
numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible
by all of the numbers from 1 to 20?

"""
import datetime


def speedTest(tests = 5):
    avgTime = 0
    for t in range(tests):
        n = datetime.datetime.now()

        a = 1
        b = 20

        multiple = 0
        unsolved = True

        ## don't check 1
        if a==1:a=2

        def multipleCheck(a, b, n):
            for v in range(a, b, 1):
                if n%v > 0: return True
            return False

        while unsolved:
            multiple += b*(b-1)
            unsolved = multipleCheck(a,b, multiple)

        m = datetime.datetime.now()
        print('multiple: %i' %(multiple))        
    avgTime += (m-n).microseconds
    return avgTime/tests

tm = speedTest(10)
print('time: %i' %(tm))
