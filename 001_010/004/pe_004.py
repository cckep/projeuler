"""
A palindromic number reads the same both ways. The largest palindrome
made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
"""

a = 999
b = 999

largest = 0

for j in range(a,1,-1):
    for k in range(b,1,-1):
        prod = j*k
        v = str(prod)
        if v == v[::-1]:
            if prod > largest: largest = prod

print(largest)
