"""
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
"""


input_number = 600851475143

def checkPrime(n):
    ## if n is even return false
    if n%2<1: return False
    ## 3 is the first valid number, n-2 is the last search term
    for i in range(3,n-2,2):
        ## return false if i is a multiple of n
        if n%i < 1: return False
    ## it is prime
    return True


def findFactors(n):
    t = n
    ## count up, so prime checks are quick
    for i in range(3,n,2):
        ## check if i is a multiple
        if t%i<1:
            ## check if i is prime
            if checkPrime(i):
                ## divide t by i, reducing search time
                t = int(t/i)
        ## if our loop value is greater than our test value, return the loop value
        if i>=t: return i
    return n

largest = findFactors(input_number)
print(largest)

