"""
A unit fraction contains 1 in the numerator. The decimal representation of the
unit fractions with denominators 2 to 10 are given:

1/2	= 	0.5
1/3	= 	0.(3)
1/4	= 	0.25
1/5	= 	0.2
1/6	= 	0.1(6)
1/7	= 	0.(142857)
1/8	= 	0.125
1/9	= 	0.(1)
1/10	= 	0.1
Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be
seen that 1/7 has a 6-digit recurring cycle.

Find the value of d < 1000 for which 1/d contains the longest recurring cycle
in its decimal fraction part.

"""

def checkSliceE(k):
    ## scans full decimal 1/7 = 0.142857142857142857 =  [1,4,2,8,5,7,1,4,...]
    ## reverse list, since the recurring cycle will be at the end
    ## ie 92833333 -> 33333829
    ## scan for matches in the sequence at most half as long (needs to be a full match)
    ## return correct sequence, reordered correctly
    k=k[::-1]
    for i,_ in enumerate(k):
        i+=1
        if i<=len(k)//2:
            if k[0:i]==k[i:i+i]:
                j=k[0:i]
                return(j[::-1])
    return([0])

def scanForward(s,a,b):
    ## verify the correct sequence by scanning forward
    ## check each digit in the sequence, with the new decimal
    ## a correct sequence must repeate (3) times
    ## that accounts for .983333678, where 33 would trigger a false positive
    for i in range(3):
        t=0
        while (a>0) & (t<len(s)):
            if s[t%len(s)] != a//b:
                return False
            a=(a%b)*10
            t+=1
    return True

def division(a,b):
    ## manually divide a/b
    ## append result to a list of digits
    ## scan digit list for recurring digits
    ## forward scan matches to verify
    ## return the correct match as a list

    seq=list([0])    ## final repeating sequence
    d=list()    ## decimal
    if a < b:a*=10 # first time strips leading '0'
    while a<b:
        a*=10
        d.append(0)
    d.append(a//b)
    a=(a%b)*10
    while (a>0) & (seq==[0]):
        d.append(a//b)
        a=(a%b)*10
        seq=checkSliceE(d)
        if (a>0) & (seq!=[0]): #(seq[0]>0) | (len(seq)>1)):
            if not scanForward(seq,a,b):
                seq=[0]
    if a==0: return list()
    return seq

long = list([0])
d=0

for i in range(2,1_001):
    r=division(1,i)
    if len(r)>=len(long):
        long,d=r,i
        print('%i set a new recurring cycle record of %i digits' %(d,len(long)))

print('1/%i has the longest recurring cycle at %i digits' %(d, len(long)))
