
"""
A perfect number is a number for which the sum of its proper divisors is
exactly equal to the number. For example, the sum of the proper divisors
of 28 would be 1+2+4+7+14=28, which means 28 is a perfect number.

A number N is called deficient if the sum of its proper divisors is less
than N and it is called abundant if this sum exceeds N.

As 12 is the smallest abundant number, 1+2+3+4+6=16, the smallest number that
can be written as the sum of two abundant numbers is 24. By mathematical
analysis, it can be shown that all integers greater than 28123 can be written
as the sum of two abundant numbers. However, this upper limit cannot be
reduced any further by analysis even though it is known that the greatest
number that cannot be expressed as the sum of two abundant numbers is less
than this limit.

Find the sum of all the positive integers which cannot be written as the sum
of two abundant numbers.
"""

def listDivisors(n):
    _n=n
    i=1
    d=set()
    while i <= _n:
        if n%i<1:
            _n=n//i
            d=d.union({i,_n})
        i+=1
    ## difference removes original element
    return d.difference({n})

def  createAbList(n):
    return [i for i in range(1,n+1) if sum(listDivisors(i))>i]

def isNotAbundantComposite(n,ab):
    for v in ab:
        if v>n: return True
        if n-v in ab: return False
    return True

print('setting up list')
n=30000
ab=createAbList(n)
solution=set()
for i in range(1,n+1):
    if i%1000<1: print(i)
    if isNotAbundantComposite(i,ab):
        solution.add(i)
print('found %i solutions' %(len(solution)))
print('the sum is:')
print(sum(solution))
