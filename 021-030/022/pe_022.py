"""
Using names.txt (right click and 'Save Link/Target As...'), a 46K text
file containing over five-thousand first names, begin by sorting it into
alphabetical order. Then working out the alphabetical value for each
name, multiply this value by its alphabetical position in the list to
obtain a name score.

For example, when the list is sorted into alphabetical order, COLIN,
which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list.
So, COLIN would obtain a score of 938 × 53 = 49714.

What is the total of all the name scores in the file?

>>>871198282

"""

import os
import codecs

path= os.path.dirname(__file__)
file= os.path.join(path, "p022_names.txt")
print(file)
alpha='abcdefghijklmnopqrstuvwxyz'
alpha=[i for i in alpha]

if os.path.isfile(file):
    ## verify the file exists
    listValue=0
    with codecs.open(file, 'r', 'utf-8') as nameList:
        names=nameList.read()
        names=names.replace('"','')
        names=names.split(',')
        names.sort() ##
        for i,n in enumerate(names):
            nameValue=sum([alpha.index(l.lower())+1 for l in n])*(i+1)
            listValue+=nameValue
else:
    print('file not found.')

print(listValue)
