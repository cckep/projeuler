"""
Let d(n) be defined as the sum of proper divisors of n (numbers less than n
which divide evenly into n)

If d(a)=b and d(b)=a where a!=b, then a and b are an amicable pair and each
of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1,2,4,5,10,11,20,22,44,55 and 110;
therefore d(220)=284. The proper divisors of 284 are 1,2,4,71 and 142; so
d(284)=220.

Evaluate the sum of all the amicable numbers under 10000

"""

def findDivisors(n):
    if n<=1: return[0]
    f=[]
    for i in range(1,n):
        if n%i<1:
            f.append(i)
    return f

def findAllAmicablePairs(n):
    checkedDict=dict()
    aP =  set()
    for i in range(1,n):
        ## look up previously tested first
        if checkedDict.get(i): a=checkedDict.get(i)
        else: a=sum(findDivisors(i))
        if checkedDict.get(a): b=checkedDict.get(a)
        else: b=sum(findDivisors(a))
        if (b==i) & (a!=b):
            print('%i & %i are a pair!' %(a,b))
            aP=aP.union({a,b})
        checkedDict.update({i:a,a:b})
    return aP

am=findAllAmicablePairs(10000)
print(am)
print(sum(list(am)))
