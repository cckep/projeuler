"""
Euler discovered the remarkable quadratic formula:
n^2+n+41

It turns out that the formula will produce 40 primes for the consecutive integer
values 0<=n<=39. However, when n=40,40^2+40+41 = 40(40+1)+41 is divisible by
41, and certainly when n=41,41^2+41+41 is clearly divisible by 41.

The incredible formula n^2-79n+1601 was discovered, which produces 80 primes for
the consecutive values 0<=n<=79. The product of the coefficients, -79 and 1601,
is -126479.

Considering quadratics of the form:
    n^2+an+b, where |a| <1000
    and |b| <=1000

    where |n| is the mdulus/absolute value of n
    e.g. |11| = 11 and |-4|=4

find the product of the coefficients, a and b, for the quadratic expression that
produces the maximum number of primes for consecutive values of n, starting
with n=0

"""

def checkPrime(n):
    ## even check
    if n<1: return False
    if n%2<1: return False
    ## only check below square root!
    l = int(n**0.5)+1
    for i in range(3,l,2):
        if n%i <1: return False
    return True

def quad(a,b,n):
    return (n*(n+a)+b) #((n**2)+(a*n)+b)

r=[0,0,0]
for a in range(-1_001,1_001,2):
    for b in range(-1_001,1_001,2): ## i think this can be limited to odd numbers?
        #if max((r[2]+a),b)%min((r[2]+a),b)!=0:
        if checkPrime(quad(a,b,r[2])):
            n=0
            while checkPrime(quad(a,b,n)):
                n+=1
            if n>=r[2]:
                print(a,b,n)
                r=[a,b,n]
#    if a%11==0:print(a)
print(r)
print(r[0]*r[1])
