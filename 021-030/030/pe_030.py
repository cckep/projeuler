"""
Surprisingly there are only three numbers that can be written as the sum of
fourth powers of their digits:

1634 = 1^4 + 6^4 + 3^4 + 4^4
8208 = 8^4 + 2^4 + 0^4 + 8^4
9474 = 9^4 + 4^4 + 7^4 + 4^4

As 1=1^4 is not a sum it is not included.

The sum of these numbers is 1634 + 8208 + 9474 = 19316

Find the sum of all the numbers that can be written as the sum of fifth powers
of their digits.

"""
p=5
powers = [i**p for i in range(0,10)]

def getSum(n):
    return sum([powers[int(i)] for i in str(n)])

#once its greater than itself, tick up by ??

def limit(j):
    i=j
    k=powers[j]
    while i<k:
        i=(i*10)+j
        k+=powers[j]
    print(k)
    return i

i=10
l=limit(9)
p=list()
while i<l:
    n = getSum(i)
    if n==i:
        p.append(i)
    i+=1

print(p)
print(sum(p))
