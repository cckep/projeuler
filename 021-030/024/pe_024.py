"""
A permutation is an ordered arrangement of objects. For example, 3124
is one possible permutation of the digits 1, 2, 3 and 4. If all of the
permutations are listed numerically or alphabetically, we call it
lexicographic order. The lexicographic permutations of 0, 1 and 2 are:

012   021   102   120   201   210

What is the millionth lexicographic permutation of the
digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?

2783915604
"""
def fact(n):
    ## returns n factorial
	f=1
	for i in range(1,n+1):
		f*=i
	return f

def lexiPerm(d,t):
    t=t-1
    ## d = digit as string
    ## t = the term to be calculated
    s=list()
    for i in range(len(d)):
        leng=fact(len(d)-1)
        a=d[((t-(t%leng))//leng)%len(d)]
        s.append(a)
        d=d.replace(str(a),'')
    s.append(d)
    return(s)

def ltoString(l):
    s=''
    for i in l:
        s+=i
    print(s)

d='0123456789'
l=lexiPerm(d,1_000_000)
s=ltoString(l)
print(s)
