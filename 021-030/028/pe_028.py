"""
starting with the number 1 and moving to the right in a clockwise direction
a 5 by 5 spiral is formed as follows:

21 22 23 24 25
20 07 08 09 10
19 06 01 02 11
18 05 04 03 12
17 16 15 14 13

It can be verified that the sum of the numbers on the diagonal are 101

What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed
in the same way

"""

"""
sides  1,3,5,7,etc
1: 1
3: 1,(3,5,7,9),(13,17,21,25),()
1,(3,2,2,2),(13,4,4,4),(31,6)

1:1
3:2
5:4
7:6
etc
"""

def diagonal(s):
    s=s-(1-s%2) #guarantee odd
    n=1
    d = [n]
    for i in range(int(s//2)):
        step=(i+1)*2
        for _ in range(4):
            n+=step
            d.append(n)
    return d

d=diagonal(1001)
#print(d)
print(sum(d))
